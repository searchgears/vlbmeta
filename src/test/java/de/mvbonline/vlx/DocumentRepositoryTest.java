package de.mvbonline.vlx;

import com.google.common.collect.Lists;
import de.mvbonline.vlx.configuration.MainSolrTestConfiguration;
import de.mvbonline.vlx.model.Document;
import de.mvbonline.vlx.repository.main.DocumentRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.solr.core.query.SolrPageRequest;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MainSolrTestConfiguration.class}, loader=SpringApplicationContextLoader.class)
public class DocumentRepositoryTest {

    @Autowired
    private DocumentRepository documentRepository;

    @Test
    public void findAllDocuments(){
        List<Document> documents = Lists.newArrayList(documentRepository.findAll());
        assertEquals(7, documents.size());
    }

    @Test
    public void findOnlyActiveDocumentsWithValidAggregatorIds(){
        FacetPage<Document> facetPage = documentRepository.findAllActiveWithAuthorFacet(new SolrPageRequest(0, 0));
        Page<FacetFieldEntry> facetResultPage = facetPage.getFacetResultPage(Document.AUTHORS_FACET_FIELD);
        assertEquals(4, facetResultPage.getTotalElements());
        assertFalse(isFacetValueInFacetResult("inactive ignored", 1, facetResultPage));
    }


    @Test
    public void facetFoldsEqualTermsTogetherIgnoringTheCase(){
        FacetPage<Document> facetPage = documentRepository.findAllActiveWithAuthorFacet(new SolrPageRequest(0, 0));
        Page<FacetFieldEntry> facetResultPage = facetPage.getFacetResultPage(Document.PUBLISHER_FACET_FIELD);
        assertTrue(isFacetValueInFacetResult("Publisher", 4, facetResultPage));
    }



    public boolean isFacetValueInFacetResult(String facetValue, int facetValueCount, Page<FacetFieldEntry> facetResultPage) {
        for (FacetFieldEntry facetFieldEntry : facetResultPage.getContent()) {
            if (facetFieldEntry.getValue().equals(facetValue) && facetFieldEntry.getValueCount() == facetValueCount){
                return true;
            }
        }
        return false;
    }

}
