package de.mvbonline.vlx.configuration;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

import javax.annotation.Resource;

@Configuration
@EnableSolrRepositories(basePackages={"de.mvbonline.vlx.repository.meta"}, multicoreSupport=true,
        solrServerRef = "metaSolrServer", solrTemplateRef = "metaSolrTemplate")
public class MetaSolrConfiguration {

    static final String SOLR_URL_VLB_META = "vlb.meta.solr";

    @Resource
    private Environment environment;


    @Bean
    @Qualifier("metaSolrServer")
    public SolrServer metaSolrServer() throws Exception{
        String solrHost = environment.getRequiredProperty(SOLR_URL_VLB_META);
        return new HttpSolrServer(solrHost);
    }

    @Bean
    @Qualifier("metaSolrTemplate")
    public SolrTemplate metaSolrTemplate(@Qualifier("metaSolrServer") SolrServer server) throws Exception {
        return new SolrTemplate(server);
    }

}


