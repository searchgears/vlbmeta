package de.mvbonline.vlx;

import de.mvbonline.vlx.repository.meta.MetaDocumentRepository;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.logging.Logger;

@SpringBootApplication
public class MetaIndexApplication {

    private static Logger LOGGER = Logger.getLogger(MetaIndexApplication.class.getName());

    public static void main(String[] args) {
        DateTime startTime = DateTime.now().withZone(DateTimeZone.UTC);
        LOGGER.info("Start to copy documents from vlb main index to vlb meta index at " + startTime + "(UTC)");

        ConfigurableApplicationContext ctx = SpringApplication.run(MetaIndexApplication.class);
        try {
            MainIndex2MetaIndexService indexService = ctx.getBean(MainIndex2MetaIndexService.class);
            indexService.fillMetaIndexFromMetaIndex();

            LOGGER.info("Delete old documents (older than " + startTime.toString() +  " UTC) from meta index");
            indexService.deleteMetaDocumentsOlderThan(startTime);

        } catch (Exception exx){
            LOGGER.severe("Meta Index creation failed with following error:  '" +  exx + "'");
        } finally {
            SpringApplication.exit(ctx);
            LOGGER.info("Application finished.");
        }

    }
}
