package de.mvbonline.vlx.repository.main;


import de.mvbonline.vlx.model.Document;
import static de.mvbonline.vlx.model.Document.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.data.solr.repository.Facet;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;


public interface DocumentRepository  extends SolrCrudRepository<Document, String> {

    String AGGREGATOR_ID_FILTER = "aggregatorId:(0 OR 5109037 OR 70601 OR 5001015)";
    String ACTIVE_FILTER = "active:true";
    int BIG_FACET_LIMIT = 40000000;
    int MEDIUM_FACET_LIMIT = 10000000;
    int SMALL_FACET_LIMIT = 1000000;

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {AUTHORS_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithAuthorFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {PUBLISHER_FACET_FIELD}, limit = SMALL_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithPublisherFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {ISBN_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithIsbnFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {ISBN10_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithIsbn10Facet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {EAN_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithEanFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {ORDER_NO_CBZ_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithCbzFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {ORDER_NO_OSB_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithOsbFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {TITLE_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithTitleFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {SET_TITLE_FACET_FIELD}, limit = SMALL_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithSetTitleFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {SERIES_TITLE_FACET_FIELD}, limit = SMALL_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithSeriesTitleFacet(Pageable page);

    @Query(value = "*:*", filters = {ACTIVE_FILTER, AGGREGATOR_ID_FILTER})
    @Facet(fields = {KEYWORDS_FACET_FIELD, GENRE_CODE_NAME_FACET_FIELD, THEMA_QUALIFIER_FACET_FIELD,
            THEMA_SUBJECT_FACET_FIELD, LANGUAGE_FACET_FIELD}, limit = MEDIUM_FACET_LIMIT)
    FacetPage<Document> findAllActiveWithUnclassifiedFacets(Pageable page);
}
