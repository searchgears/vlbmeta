package de.mvbonline.vlx.repository.meta;


import org.joda.time.DateTime;

public interface MetaDocumentRepositoryCustom {

    void eraseOldDocuments(DateTime dateTime);
}
