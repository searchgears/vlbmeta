package de.mvbonline.vlx.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;

import java.util.Date;


public class MetaDocument {

    public static final String INDEX_DATE_FIELD = "indexDate";

    @Id
    private String id;

    @Field(INDEX_DATE_FIELD)
    private DateTime indexDate = DateTime.now();

    @Field
    private String author;  //The Author Index (AU)

    @Field("AUPos")
    private Integer alphabeticPositionAuthor;

    @Field
    private String publisher;  //The Publisher Index (VL)

    @Field("VLPos")
    private Integer alphabeticPositionPublisher;

    @Field
    private String setOrSeriesTitle;  //Set and Series Index (RH)

    @Field("RHPos")
    private Integer alphabeticPositionSetOrSeriesTitle;

    @Field
    private String title;  // Title Index (TI)

    @Field("TIPos")
    private Integer alphabeticPositionTitle;

    @Field
    private String identifier;  // Identifier Index (IS)

    @Field("ISPos")
    private Integer alphabeticPositionIdentifier;

    @Field
    private String keyword;  // Keyword Index (SW)

    @Field("SWPos")
    private Integer alphabeticPositionKeyword;

    @Field
    private String classification;  // Classification Index (WG)

    @Field("WGPos")
    private Integer alphabeticPositionClassification;

    @Field
    private String ddcKeyword;  // DDC Index (DW)

    @Field("DWPos")
    private Integer alphabeticPositionDdcKeyword;

    @Field
    private String themaSubject;  // Thema Subject Index (TS)

    @Field("TSPos")
    private Integer alphabeticPositionThemaSubject;

    @Field
    private String themaQualifier;  // Thema Qualifier Index (TQ)

    @Field("TQPos")
    private Integer alphabeticPositionThemaQualifier;

    @Field
    private String language;  // Language Index (LA|SP)

    @Field("LAPos")
    private Integer alphabeticPositionLanguage;

    @Field
    private int counter;


    public int getAlphabeticPositionThemaQualifier() {
        return alphabeticPositionThemaQualifier;
    }


    public void setAlphabeticPositionThemaQualifier(int alphabeticPositionThemaQualifier) {
        this.alphabeticPositionThemaQualifier = alphabeticPositionThemaQualifier;
    }


    public String getAuthor() {
        return author;
    }


    public void setAuthor(String author) {
        this.author = author;
    }


    public int getAlphabeticPositionAuthor() {
        return alphabeticPositionAuthor;
    }


    public void setAlphabeticPositionAuthor(int alphabeticPositionAuthor) {
        this.alphabeticPositionAuthor = alphabeticPositionAuthor;
    }


    public String getPublisher() {
        return publisher;
    }


    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }


    public int getAlphabeticPositionPublisher() {
        return alphabeticPositionPublisher;
    }


    public void setAlphabeticPositionPublisher(int alphabeticPositionPublisher) {
        this.alphabeticPositionPublisher = alphabeticPositionPublisher;
    }


    public String getSetOrSeriesTitle() {
        return setOrSeriesTitle;
    }


    public void setSetOrSeriesTitle(String setOrSeriesTitle) {
        this.setOrSeriesTitle = setOrSeriesTitle;
    }


    public int getAlphabeticPositionSetOrSeriesTitle() {
        return alphabeticPositionSetOrSeriesTitle;
    }


    public void setAlphabeticPositionSetOrSeriesTitle(int alphabeticPositionSetOrSeriesTitle) {
        this.alphabeticPositionSetOrSeriesTitle = alphabeticPositionSetOrSeriesTitle;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public int getAlphabeticPositionTitle() {
        return alphabeticPositionTitle;
    }


    public void setAlphabeticPositionTitle(int alphabeticPositionTitle) {
        this.alphabeticPositionTitle = alphabeticPositionTitle;
    }


    public String getIdentifier() {
        return identifier;
    }


    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }


    public int getAlphabeticPositionIdentifier() {
        return alphabeticPositionIdentifier;
    }


    public void setAlphabeticPositionIdentifier(int alphabeticPositionIdentifier) {
        this.alphabeticPositionIdentifier = alphabeticPositionIdentifier;
    }


    public String getKeyword() {
        return keyword;
    }


    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }


    public int getAlphabeticPositionKeyword() {
        return alphabeticPositionKeyword;
    }


    public void setAlphabeticPositionKeyword(int alphabeticPositionKeyword) {
        this.alphabeticPositionKeyword = alphabeticPositionKeyword;
    }


    public String getClassification() {
        return classification;
    }


    public void setClassification(String classification) {
        this.classification = classification;
    }


    public int getAlphabeticPositionClassification() {
        return alphabeticPositionClassification;
    }


    public void setAlphabeticPositionClassification(int alphabeticPositionClassification) {
        this.alphabeticPositionClassification = alphabeticPositionClassification;
    }


    public String getDdcKeyword() {
        return ddcKeyword;
    }


    public void setDdcKeyword(String ddcKeyword) {
        this.ddcKeyword = ddcKeyword;
    }


    public int getAlphabeticPositionDdcKeyword() {
        return alphabeticPositionDdcKeyword;
    }


    public void setAlphabeticPositionDdcKeyword(int alphabeticPositionDdcKeyword) {
        this.alphabeticPositionDdcKeyword = alphabeticPositionDdcKeyword;
    }


    public String getThemaSubject() {
        return themaSubject;
    }


    public void setThemaSubject(String themaSubject) {
        this.themaSubject = themaSubject;
    }


    public int getAlphabeticPositionThemaSubject() {
        return alphabeticPositionThemaSubject;
    }


    public void setAlphabeticPositionThemaSubject(int alphabeticPositionThemaSubject) {
        this.alphabeticPositionThemaSubject = alphabeticPositionThemaSubject;
    }


    public String getThemaQualifier() {
        return themaQualifier;
    }


    public void setThemaQualifier(String themaQualifier) {
        this.themaQualifier = themaQualifier;
    }


    public String getLanguage() {
        return language;
    }


    public void setLanguage(String language) {
        this.language = language;
    }


    public int getAlphabeticPositionLanguage() {
        return alphabeticPositionLanguage;
    }


    public void setAlphabeticPositionLanguage(int alphabeticPositionLanguage) {
        this.alphabeticPositionLanguage = alphabeticPositionLanguage;
    }


    public int getCounter() {
        return counter;
    }


    public void setCounter(int counter) {
        this.counter = counter;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this);
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "indexDate");
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "indexDate");
    }


    public DateTime getIndexDate() {
        return indexDate;
    }


    public void setIndexDate(DateTime indexDate) {
        this.indexDate = indexDate;
    }
}
