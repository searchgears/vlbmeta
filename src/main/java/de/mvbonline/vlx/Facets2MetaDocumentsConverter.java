package de.mvbonline.vlx;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.ibm.icu.text.Collator;
import com.ibm.icu.text.RuleBasedCollator;
import com.ibm.icu.util.ULocale;
import de.mvbonline.vlx.model.MetaDocument;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.FacetPage;
import java.util.*;
import java.util.logging.Logger;


public abstract class Facets2MetaDocumentsConverter {

    private static Logger LOGGER = Logger.getLogger(Facets2MetaDocumentsConverter.class.getName());

    Comparator<FacetFieldEntry> alphabeticalComparator = new AlphabeticalFacetComparator();

    protected abstract void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument);


    public List<MetaDocument> convert(List<FacetFieldEntry> allFacetEntries){


        Collections.sort(allFacetEntries, alphabeticalComparator);
        int alphabeticalPosition = 1;

        List<MetaDocument> metaDocuments = Lists.newArrayList();
        Set<String> facetValuesSeen = Sets.newHashSet();

        for (FacetFieldEntry facetFieldEntry : allFacetEntries) {

            String facetValueNormalized = StringUtils.lowerCase(facetFieldEntry.getValue()).trim();
            if (facetValuesSeen.contains(facetValueNormalized)) {
                continue;
            } else {
                facetValuesSeen.add(facetValueNormalized);
            }

            MetaDocument metaDocument = createMetaDocument(facetFieldEntry, alphabeticalPosition);
            metaDocuments.add(metaDocument);
            alphabeticalPosition++;
        }
        return metaDocuments;
    }





    private MetaDocument createMetaDocument(FacetFieldEntry facetFieldEntry, int alphabeticalPosition) {
        String facetValue = facetFieldEntry.getValue();
        int facetValueCount = (int) facetFieldEntry.getValueCount();

        MetaDocument metaDocument = new MetaDocument();
        metaDocument.setId(facetFieldEntry.getField().getName() + "_" + facetValue);

        setValueAndAlphabeticalPositionInMetaDocument(facetValue, alphabeticalPosition, metaDocument);

        metaDocument.setCounter(facetValueCount);
        return metaDocument;
    }


    class AlphabeticalFacetComparator implements Comparator<FacetFieldEntry>{

        private RuleBasedCollator collator;

        public AlphabeticalFacetComparator()  {
            RuleBasedCollator baseCollator = (RuleBasedCollator) Collator.getInstance(new ULocale("de_DE"));

            String DIN5007_2_tailorings =
                    "& ae , a\u0308 & AE , A\u0308"+
                           "& oe , o\u0308 & OE , O\u0308"+
                            "& ue , u\u0308 & UE , u\u0308";

            try {
                collator = new RuleBasedCollator(baseCollator.getRules() + DIN5007_2_tailorings);
            } catch (Exception e) {
                throw new IllegalArgumentException("Error creating din 5007 collator", e);
            }
        }


        @Override
        public int compare(FacetFieldEntry entry, FacetFieldEntry otherEntry) {
            return collator.compare(entry.getValue(), otherEntry.getValue());
        }
    }



}
