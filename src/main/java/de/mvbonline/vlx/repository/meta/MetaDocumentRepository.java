package de.mvbonline.vlx.repository.meta;

import de.mvbonline.vlx.model.MetaDocument;
import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;

import java.util.List;


public interface MetaDocumentRepository extends MetaDocumentRepositoryCustom, SolrCrudRepository<MetaDocument, String> {

    @Query(value = "?0:?1")
    List<MetaDocument> findInField(String field, String query);
}
