package de.mvbonline.vlx.configuration;

import de.mvbonline.vlx.SolrTestUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;
import org.springframework.data.solr.server.support.EmbeddedSolrServerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;


@Configuration
public class MainSolrTestConfiguration extends MainSolrConfiguration {


    @Override
    @Bean
    @Qualifier("mainSolrServer")
    public SolrServer mainSolrServer() throws Exception {
        return SolrTestUtils.getEmbeddedSolrServer("main_solr_home", MainSolrTestConfiguration.class);
    }


    @Bean
    public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {
        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
        factory.setResources(new Resource[] {new ClassPathResource("documents.json")});
        return factory;
    }
}
