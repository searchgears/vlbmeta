package de.mvbonline.vlx;

import org.apache.solr.client.solrj.SolrServer;
import org.springframework.data.solr.server.support.EmbeddedSolrServerFactory;
import org.xml.sax.SAXException;

import javax.management.MBeanServer;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class SolrTestUtils {

    public static SolrServer getEmbeddedSolrServer(String solr_home, Class classForResource) throws URISyntaxException, ParserConfigurationException, IOException, SAXException {
        URL solrConfigurationResource = classForResource.getClassLoader().getResource(solr_home);
        String solrConfigurationPath = solrConfigurationResource.toURI().getPath();
        EmbeddedSolrServerFactory factory = new EmbeddedSolrServerFactory(solrConfigurationPath);
        return factory.getSolrServer();
    }
}
