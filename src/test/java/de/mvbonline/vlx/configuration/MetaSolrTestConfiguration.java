package de.mvbonline.vlx.configuration;

import de.mvbonline.vlx.SolrTestUtils;
import org.apache.solr.client.solrj.SolrServer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;
import org.springframework.data.solr.server.support.EmbeddedSolrServerFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;


//needs -Dsolr.allow.unsafe.resourceloading=true
@Configuration
public class MetaSolrTestConfiguration extends MetaSolrConfiguration {

    @Override
    @Bean
    @Qualifier("metaSolrServer")
    public SolrServer metaSolrServer() throws Exception {
        return SolrTestUtils.getEmbeddedSolrServer("meta_solr_home", MetaSolrTestConfiguration.class);
    }
}
