package de.mvbonline.vlx;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import de.mvbonline.vlx.model.Document;
import de.mvbonline.vlx.model.MetaDocument;
import de.mvbonline.vlx.repository.main.DocumentRepository;
import de.mvbonline.vlx.repository.meta.MetaDocumentRepository;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.solr.core.query.SolrPageRequest;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.FacetPage;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import static de.mvbonline.vlx.model.Document.*;


//TODO DDC?
//TODO shall counter be merged for identifier and setOrSeriesTitle?
@Service
public class MainIndex2MetaIndexService {

    public static final int DEFAULT_BATCH_SIZE = 5000;
    public static final SolrPageRequest ZERO_ROWS = new SolrPageRequest(0, 0);
    private static Logger LOGGER = Logger.getLogger(MainIndex2MetaIndexService.class.getName());

    private DocumentRepository documentRepository;
    private MetaDocumentRepository metaDocumentRepository;
    private int batchSize;


    @Autowired
    public MainIndex2MetaIndexService(DocumentRepository documentRepository,
                                      MetaDocumentRepository metaDocumentRepository) {
        this(documentRepository, metaDocumentRepository, DEFAULT_BATCH_SIZE);
    }


    public MainIndex2MetaIndexService(DocumentRepository documentRepository,
                                      MetaDocumentRepository metaDocumentRepository,
                                      int batchSize) {
        this.documentRepository = documentRepository;
        this.metaDocumentRepository = metaDocumentRepository;
        this.batchSize = batchSize;
    }


    public void fillMetaIndexFromMetaIndex() {

        LOGGER.info("Getting facets for author and publisher from vlb main index...");
        FacetPage<Document> facetPageAuthors = documentRepository.findAllActiveWithAuthorFacet(ZERO_ROWS);

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setAuthor(value);
                metaDocument.setAlphabeticPositionAuthor(alphabeticalPosition);
            }
        }, Lists.newArrayList(AUTHORS_FACET_FIELD), facetPageAuthors);

        FacetPage<Document> facetPagePublisher = documentRepository.findAllActiveWithPublisherFacet(ZERO_ROWS);
        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setPublisher(value);
                metaDocument.setAlphabeticPositionPublisher(alphabeticalPosition);
            }
        }, Lists.newArrayList(PUBLISHER_FACET_FIELD), facetPagePublisher);

        LOGGER.info("Facets for author and publisher copied to vlb meta index.");

        LOGGER.info("Getting facets for title fields from vlb main index...");

        Facets2MetaDocumentsConverter setAndSeriesTitleFacets2MetaDocumentsConverter = new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setSetOrSeriesTitle(value);
                metaDocument.setAlphabeticPositionSetOrSeriesTitle(alphabeticalPosition);
            }
        };

        FacetPage<Document> facetPageSetTitle = documentRepository.findAllActiveWithSetTitleFacet(ZERO_ROWS);
        FacetPage<Document> facetPageSeriesTitle = documentRepository.findAllActiveWithSeriesTitleFacet(ZERO_ROWS);
        saveFacet2MetaIndex(setAndSeriesTitleFacets2MetaDocumentsConverter, Lists.newArrayList(SERIES_TITLE_FACET_FIELD, SET_TITLE_FACET_FIELD),
                facetPageSetTitle, facetPageSeriesTitle);


        FacetPage<Document> facetPageTitle = documentRepository.findAllActiveWithTitleFacet(ZERO_ROWS);

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setTitle(value);
                metaDocument.setAlphabeticPositionTitle(alphabeticalPosition);
            }
        }, Lists.newArrayList(TITLE_FACET_FIELD), facetPageTitle);

        LOGGER.info("Facets for title fields copied to vlb meta index.");

        LOGGER.info("Getting facets for id fields from vlb main index...");
        copyIdsToMetaIndex();

        LOGGER.info("Facets for id fields copied to vlb meta index.");

        LOGGER.info("Getting facets for remaining fields from vlb main index...");

        FacetPage<Document> facetPageVariousFields = documentRepository.findAllActiveWithUnclassifiedFacets(new SolrPageRequest(0, 0));

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setKeyword(value);
                metaDocument.setAlphabeticPositionKeyword(alphabeticalPosition);
            }
        },  Lists.newArrayList(KEYWORDS_FACET_FIELD), facetPageVariousFields);

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setClassification(value);
                metaDocument.setAlphabeticPositionClassification(alphabeticalPosition);
            }
        }, Lists.newArrayList(GENRE_CODE_NAME_FACET_FIELD), facetPageVariousFields);

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setThemaSubject(value);
                metaDocument.setAlphabeticPositionThemaSubject(alphabeticalPosition);
            }
        }, Lists.newArrayList(THEMA_SUBJECT_FACET_FIELD), facetPageVariousFields);

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setThemaQualifier(value);
                metaDocument.setAlphabeticPositionThemaQualifier(alphabeticalPosition);
            }
        }, Lists.newArrayList(THEMA_QUALIFIER_FACET_FIELD), facetPageVariousFields);

        saveFacet2MetaIndex(new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setLanguage(value);
                metaDocument.setAlphabeticPositionLanguage(alphabeticalPosition);
            }
        }, Lists.newArrayList(LANGUAGE_FACET_FIELD), facetPageVariousFields);

        LOGGER.info("Facets for remaining fields copied to vlb meta index.");
    }


    private void copyIdsToMetaIndex() {

        Facets2MetaDocumentsConverter identifierFacets2MetaDocumentsConverter = new Facets2MetaDocumentsConverter() {
            @Override
            protected void setValueAndAlphabeticalPositionInMetaDocument(String value, int alphabeticalPosition, MetaDocument metaDocument) {
                metaDocument.setIdentifier(value);
                metaDocument.setAlphabeticPositionIdentifier(alphabeticalPosition);
            }
        };

        FacetPage<Document> facetPageIsbn = documentRepository.findAllActiveWithIsbnFacet(ZERO_ROWS);
        FacetPage<Document> facetPageIsbn10 = documentRepository.findAllActiveWithIsbn10Facet(ZERO_ROWS);
        FacetPage<Document> facetPageEan = documentRepository.findAllActiveWithEanFacet(ZERO_ROWS);
        FacetPage<Document> facetPageOsb = documentRepository.findAllActiveWithOsbFacet(ZERO_ROWS);
        FacetPage<Document> facetPageCbz = documentRepository.findAllActiveWithCbzFacet(ZERO_ROWS);

        saveFacet2MetaIndex(identifierFacets2MetaDocumentsConverter,
                Lists.newArrayList(ISBN_FACET_FIELD, ISBN10_FACET_FIELD, EAN_FACET_FIELD, ORDER_NO_OSB_FACET_FIELD, ORDER_NO_CBZ_FACET_FIELD),
                facetPageIsbn, facetPageIsbn10, facetPageEan, facetPageOsb, facetPageCbz);
    }


    private void saveFacet2MetaIndex(Facets2MetaDocumentsConverter facet2MetaDocumentsConverter, List<String> facetFields, FacetPage<Document>... facetPages) {

        List<FacetFieldEntry> allFacetEntries;
        allFacetEntries = getFacetFieldEntries(facetFields, facetPages);

        List<MetaDocument> metaDocuments = facet2MetaDocumentsConverter.convert(allFacetEntries);
        LOGGER.info("Found " + metaDocuments.size() + " meta documents to index for main index field(s) '" + Joiner.on(" + ").join(facetFields) + "'");
        List<MetaDocument> batch = Lists.newArrayList();
        for (MetaDocument metaDocument : metaDocuments) {
            batch.add(metaDocument);
            if (batch.size() >= batchSize) {
                metaDocumentRepository.save(batch);
                batch.clear();
            }
        }
        if (!batch.isEmpty()) {
            metaDocumentRepository.save(batch);
        }
        metaDocuments.clear();
    }

    private List<FacetFieldEntry> getFacetFieldEntries(List<String> facetFields, FacetPage... facetPages) {
        List<FacetFieldEntry> allFacetEntries = Lists.newArrayList();

        for (FacetPage facetPage : facetPages) {

            for (String facetField : facetFields) {

                Page facetResultPage = facetPage.getFacetResultPage(facetField);

                List<FacetFieldEntry> facetEntries = facetResultPage.getContent();
                if (facetEntries.isEmpty()) {
                    continue;
                }
                allFacetEntries.addAll(facetEntries);
            }
        }
        if (allFacetEntries.isEmpty()){
            LOGGER.warning("No facets could be found for facet fields " + facetFields);
        }
        return allFacetEntries;
    }


    public void deleteMetaDocumentsOlderThan(DateTime dateTime) {
        metaDocumentRepository.eraseOldDocuments(dateTime);
    }

}
