package de.mvbonline.vlx.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;

public class Document {

    public static final String AUTHORS_FACET_FIELD = "authors_facet";
    public static final String PUBLISHER_FACET_FIELD = "publisher_facet";
    public static final String SET_TITLE_FACET_FIELD = "setTitle_facet";
    public static final String SERIES_TITLE_FACET_FIELD = "seriesTitle_facet";
    public static final String TITLE_FACET_FIELD = "title_facet";
    public static final String KEYWORDS_FACET_FIELD = "keywords_facet";
    public static final String GENRE_CODE_NAME_FACET_FIELD = "genreCodeName_facet";
    public static final String THEMA_SUBJECT_FACET_FIELD = "themaSubject";
    public static final String THEMA_QUALIFIER_FACET_FIELD = "themaQualifier";
    public static final String LANGUAGE_FACET_FIELD = "language";
    public static final String ISBN_FACET_FIELD = "isbn";
    public static final String ISBN10_FACET_FIELD = "isbn10";
    public static final String EAN_FACET_FIELD = "ean";
    public static final String ORDER_NO_OSB_FACET_FIELD = "orderNoOsb";
    public static final String ORDER_NO_CBZ_FACET_FIELD = "orderNoCbz";


    @Id
    private String id;

    @Field
    private String active;

    @Field
    private String type;

    @Field
    private int typeSort;

    @Field
    private int aggregatorId;

    @Field(AUTHORS_FACET_FIELD)
    @JsonProperty(AUTHORS_FACET_FIELD)
    private String[] authorsFacet;

    @Field(PUBLISHER_FACET_FIELD)
    @JsonProperty(PUBLISHER_FACET_FIELD)
    private String[] publisherFacet;

    @Field(SET_TITLE_FACET_FIELD)
    @JsonProperty(SET_TITLE_FACET_FIELD)
    private String setTitleFacet;

    @Field(SERIES_TITLE_FACET_FIELD)
    @JsonProperty(SERIES_TITLE_FACET_FIELD)
    private String[] seriesTitleFacet;

    @Field(TITLE_FACET_FIELD)
    @JsonProperty(TITLE_FACET_FIELD)
    private String titleFacet;

    @Field(KEYWORDS_FACET_FIELD)
    @JsonProperty(KEYWORDS_FACET_FIELD)
    private String[] keywordsFacet;

    @Field(GENRE_CODE_NAME_FACET_FIELD)
    @JsonProperty(GENRE_CODE_NAME_FACET_FIELD)
    private String genreCodeNameFacet;

    @Field(THEMA_SUBJECT_FACET_FIELD)
    @JsonProperty(THEMA_SUBJECT_FACET_FIELD)
    private String[] themaSubject;

    @Field(THEMA_QUALIFIER_FACET_FIELD)
    @JsonProperty(THEMA_QUALIFIER_FACET_FIELD)
    private String[] themaQualifier;

    @Field(LANGUAGE_FACET_FIELD)
    @JsonProperty(LANGUAGE_FACET_FIELD)
    private String language;

    @Field(ISBN_FACET_FIELD)
    @JsonProperty(ISBN_FACET_FIELD)
    private String isbn;

    @Field(ISBN10_FACET_FIELD)
    @JsonProperty(ISBN10_FACET_FIELD)
    private String isbn10;

    @Field(EAN_FACET_FIELD)
    @JsonProperty(EAN_FACET_FIELD)
    private String ean;

    @Field(ORDER_NO_OSB_FACET_FIELD)
    @JsonProperty(ORDER_NO_OSB_FACET_FIELD)
    private String orderNoOsb;

    @Field(ORDER_NO_CBZ_FACET_FIELD)
    @JsonProperty(ORDER_NO_CBZ_FACET_FIELD)
    private String orderNoCbz;


    public String[] getAuthorsFacet() {
        return authorsFacet;
    }


    public void setAuthorsFacet(String[] authorsFacet) {
        this.authorsFacet = authorsFacet;
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getActive() {
        return active;
    }


    public void setActive(String active) {
        this.active = active;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public int getTypeSort() {
        return typeSort;
    }


    public void setTypeSort(int typeSort) {
        this.typeSort = typeSort;
    }


    public int getAggregatorId() {
        return aggregatorId;
    }


    public void setAggregatorId(int aggregatorId) {
        this.aggregatorId = aggregatorId;
    }


    @Override
    public String toString() {
        return ReflectionToStringBuilder.reflectionToString(this);
    }


    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }


    public String[] getPublisherFacet() {
        return publisherFacet;
    }


    public void setPublisherFacet(String[] publisherFacet) {
        this.publisherFacet = publisherFacet;
    }


    public String getSetTitleFacet() {
        return setTitleFacet;
    }


    public void setSetTitleFacet(String setTitleFacet) {
        this.setTitleFacet = setTitleFacet;
    }


    public String[] getSeriesTitleFacet() {
        return seriesTitleFacet;
    }


    public void setSeriesTitleFacet(String[] seriesTitleFacet) {
        this.seriesTitleFacet = seriesTitleFacet;
    }


    public String[] getKeywordsFacet() {
        return keywordsFacet;
    }


    public void setKeywordsFacet(String[] keywordsFacet) {
        this.keywordsFacet = keywordsFacet;
    }


    public String getTitleFacet() {
        return titleFacet;
    }


    public void setTitleFacet(String titleFacet) {
        this.titleFacet = titleFacet;
    }


    public String getLanguage() {
        return language;
    }


    public void setLanguage(String language) {
        this.language = language;
    }


    public String[] getThemaQualifier() {
        return themaQualifier;
    }


    public void setThemaQualifier(String[] themaQualifier) {
        this.themaQualifier = themaQualifier;
    }


    public String[] getThemaSubject() {
        return themaSubject;
    }


    public void setThemaSubject(String[] themaSubject) {
        this.themaSubject = themaSubject;
    }


    public String getGenreCodeNameFacet() {
        return genreCodeNameFacet;
    }


    public void setGenreCodeNameFacet(String genreCodeNameFacet) {
        this.genreCodeNameFacet = genreCodeNameFacet;
    }


    public String getOrderNoCbz() {
        return orderNoCbz;
    }


    public void setOrderNoCbz(String orderNoCbz) {
        this.orderNoCbz = orderNoCbz;
    }


    public String getOrderNoOsb() {
        return orderNoOsb;
    }


    public void setOrderNoOsb(String orderNoOsb) {
        this.orderNoOsb = orderNoOsb;
    }


    public String getEan() {
        return ean;
    }


    public void setEan(String ean) {
        this.ean = ean;
    }


    public String getIsbn10() {
        return isbn10;
    }


    public void setIsbn10(String isbn10) {
        this.isbn10 = isbn10;
    }


    public String getIsbn() {
        return isbn;
    }


    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
}
