package de.mvbonline.vlx;

import de.mvbonline.vlx.configuration.MainSolrTestConfiguration;
import de.mvbonline.vlx.configuration.MetaSolrTestConfiguration;
import de.mvbonline.vlx.model.MetaDocument;
import de.mvbonline.vlx.repository.main.DocumentRepository;
import de.mvbonline.vlx.repository.meta.MetaDocumentRepository;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {MainSolrTestConfiguration.class, MetaSolrTestConfiguration.class}, loader=SpringApplicationContextLoader.class)
public class MainIndex2MetaIndexServiceTest {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private MetaDocumentRepository metaDocumentRepository;

    private MainIndex2MetaIndexService mainIndex2MetaIndexService;


    @Before
    public void createVlbIndexService() {
        mainIndex2MetaIndexService =  new MainIndex2MetaIndexService(documentRepository, metaDocumentRepository, 5);
        metaDocumentRepository.deleteAll();
        assertNumberOfDocumentsInMetaRepository(0);
        mainIndex2MetaIndexService.fillMetaIndexFromMetaIndex();
    }


    @After
    public void deleteMetaDocuments(){
        metaDocumentRepository.deleteAll();
    }


    @Test
    public void checkTotalResultSizeInMetaIndex() {
        assertNumberOfDocumentsInMetaRepository(23);
    }


    @Test
    public void checkThatOnlyOneFieldIsSetInMetaIndex() {
        List<MetaDocument> metaDocuments = metaDocumentRepository.findInField("classification", "genre");
        assertEquals(1, metaDocuments.size());
        MetaDocument metaDocument = metaDocuments.get(0);

        MetaDocument expectedMetaDocument = new MetaDocument();
        expectedMetaDocument.setId("genreCodeName_facet_Genre");
        expectedMetaDocument.setClassification("Genre");
        expectedMetaDocument.setAlphabeticPositionClassification(1);
        expectedMetaDocument.setCounter(2);

        assertEquals(expectedMetaDocument, metaDocument);
    }


    @Test
    public void seriesAndSetTitleIsDeduplicated() {
        assertEquals(1, metaDocumentRepository.findInField("setOrSeriesTitle", "SetTitle").size());
    }


    @Test
    public void identifierWildcard() {
        List<MetaDocument> documents = metaDocumentRepository.findInField("identifier", "*");
        assertEquals(6, documents.size());
    }

    @Test
    public void minusIsRemovedFromWildcard() {
        List<MetaDocument> documents = metaDocumentRepository.findInField("identifier", "1-2*");
        assertEquals(1, documents.size());
    }


    @Test
    public void identifierIsDeduplicated() {
        List<MetaDocument> documents = metaDocumentRepository.findInField("identifier", "1*");
        assertEquals(2, documents.size());
        MetaDocument firstDocument = documents.get(0);
        assertEquals("1", firstDocument.getIdentifier());
        assertEquals(1, firstDocument.getCounter());
        MetaDocument secondDocument = documents.get(1);
        assertEquals("123", secondDocument.getIdentifier());
        assertEquals(1, secondDocument.getCounter());
    }


    @Test
    public void multiValueFacetIsDeduplicated() {
        assertEquals(1, metaDocumentRepository.findInField("keyword", "Keyword2").size());
        assertEquals(1, metaDocumentRepository.findInField("keyword", "keyword2").size());
    }


    @Test
    public void firstAlphabeticPositionIsSetCorrectly() {
        List<MetaDocument> firstRankDocuments = metaDocumentRepository.findInField("AUPos", "1");
        assertEquals(1, firstRankDocuments.size());
        MetaDocument document = firstRankDocuments.get(0);
        assertEquals("Abc", document.getAuthor());
        assertEquals(1, document.getAlphabeticPositionAuthor());
        assertEquals(1, document.getCounter());
    }


    @Test
    public void checkForNonExistingRank() {
        assertEquals(0, metaDocumentRepository.findInField("AUPos", "5").size());
    }


    @Test
    public void checkThatAlphabeticPositionsAreNeverZero() {
        List<MetaDocument> metaDocuments = metaDocumentRepository.findInField("AUPos", "0");
        assertEquals(0, metaDocuments.size());
    }


    @Test
    public void alphabeticPositionStartsWithOneForEachField() {
        List<MetaDocument> firstRankDocuments = metaDocumentRepository.findInField("language", "al");
        assertEquals(1, firstRankDocuments.size());
        assertEquals(1, firstRankDocuments.get(0).getAlphabeticPositionLanguage());

        firstRankDocuments = metaDocumentRepository.findInField("setOrSeriesTitle", "SeriesTitle");
        assertEquals(1, firstRankDocuments.size());
        assertEquals(1, firstRankDocuments.get(0).getAlphabeticPositionSetOrSeriesTitle());
    }


    @Test
    public void alphabeticPositionIsContinousWithinComposedField() {
        List<MetaDocument> documents = metaDocumentRepository.findInField("setOrSeriesTitle", "SetTitle");
        assertEquals(1, documents.size());
        assertEquals(2, documents.get(0).getAlphabeticPositionSetOrSeriesTitle());
    }


    @Test
    public void alphabeticPositionWorksForNumericIdentifier() {
        List<MetaDocument> documents = metaDocumentRepository.findInField("identifier", "0123");
        assertEquals(1, documents.size());
        assertEquals(1, documents.get(0).getAlphabeticPositionIdentifier());

        documents = metaDocumentRepository.findInField("ISPos", "2");
        assertEquals(1, documents.size());
        assertEquals("1", documents.get(0).getIdentifier());
    }


    @Test
    public void counterIsSetCorrectly() {
        List<MetaDocument> firstRankDocuments = metaDocumentRepository.findInField("publisher", "Publisher");
        assertEquals(1, firstRankDocuments.size());
        assertEquals(4, firstRankDocuments.get(0).getCounter());
    }


    @Test
    public void prefixQueryWorks() {
        assertEquals(2, metaDocumentRepository.findInField("themaSubject", "thema*").size());
    }


    @Test
    public void nothingDeletedIfIndexDateIsInThePast() {
        mainIndex2MetaIndexService.deleteMetaDocumentsOlderThan(new DateTime(2014, 1, 1, 0, 0));
        checkTotalResultSizeInMetaIndex();
    }


    @Test
    public void everyThingDeletedIfIndexDateIsInTheFuture() {
        mainIndex2MetaIndexService.deleteMetaDocumentsOlderThan(new DateTime(2100, 1, 1, 0, 0));
        assertNumberOfDocumentsInMetaRepository(0);
    }


    private void assertNumberOfDocumentsInMetaRepository(int numberOfDocuments) {
        assertEquals(numberOfDocuments, metaDocumentRepository.count());
    }


}
