package de.mvbonline.vlx.repository.meta;

import de.mvbonline.vlx.model.MetaDocument;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;


@Repository
public class MetaDocumentRepositoryImpl implements MetaDocumentRepositoryCustom {

        @Autowired
        private SolrTemplate metaSolrTemplate;

        @Override
        public void eraseOldDocuments(DateTime dateTime) {
            metaSolrTemplate.delete(new SimpleQuery(Criteria.where(MetaDocument.INDEX_DATE_FIELD).lessThan(dateTime)));
            metaSolrTemplate.commit();
        }
}

