package de.mvbonline.vlx;

import com.google.common.collect.Lists;
import de.mvbonline.vlx.model.MetaDocument;
import org.junit.Test;
import org.springframework.data.solr.core.query.SimpleField;
import org.springframework.data.solr.core.query.result.FacetFieldEntry;
import org.springframework.data.solr.core.query.result.SimpleFacetFieldEntry;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Facets2MetaDocumentsConverterTest {

    private static final List<FacetFieldEntry> facetFieldEntriesForAuthor = Lists.newArrayList();

    private static final List<FacetFieldEntry> facetFieldEntriesForPublisher = Lists.newArrayList();

    static {
        facetFieldEntriesForAuthor.add(new SimpleFacetFieldEntry(new SimpleField("author"), "Aa", 1));
        facetFieldEntriesForAuthor.add(new SimpleFacetFieldEntry(new SimpleField("author"), "Ac", 3));
        facetFieldEntriesForAuthor.add(new SimpleFacetFieldEntry(new SimpleField("author"), "ab", 5));
        facetFieldEntriesForAuthor.add(new SimpleFacetFieldEntry(new SimpleField("author"), "Ab ", 1));
        facetFieldEntriesForAuthor.add(new SimpleFacetFieldEntry(new SimpleField("author"), "aÄb", 5));
        facetFieldEntriesForAuthor.add(new SimpleFacetFieldEntry(new SimpleField("author"), "aäa", 5));

        facetFieldEntriesForPublisher.add(new SimpleFacetFieldEntry(new SimpleField("publisher"), "ab", 10));
        facetFieldEntriesForPublisher.add(new SimpleFacetFieldEntry(new SimpleField("publisher"), "aaÄä", 2));
    }

    private Facets2MetaDocumentsConverter authorConverter = new Facets2MetaDocumentsConverter() {

        @Override
        protected void setValueAndAlphabeticalPositionInMetaDocument(String value,
        int alphabeticalPosition, MetaDocument metaDocument) {
            metaDocument.setAuthor(value);
            metaDocument.setAlphabeticPositionAuthor(alphabeticalPosition);
        }
    };;


    @Test
    public void emptyFacetsLeadToEmptyMetaDocuments(){
        List<MetaDocument> documents = convert(Collections.<FacetFieldEntry>emptyList());
        assertTrue(documents.isEmpty());
    }


    @Test
    public void checkMetaDocumentsForSingleSimpleFacetField(){
        List<MetaDocument> documents = convert(facetFieldEntriesForPublisher);

        List<MetaDocument> expectedMetaDocuments = Lists.newArrayList(metaDocument("publisher_aaÄä", "aaÄä", 2, 1),
                metaDocument("publisher_ab", "ab", 10, 2));
        assertEquals(expectedMetaDocuments, documents);
    }


    @Test
    public void checkMetaDocumentsForSingleComplexFacetField(){
        List<MetaDocument> documents = convert(facetFieldEntriesForAuthor);

        List<MetaDocument> expectedMetaDocuments = Lists.newArrayList(metaDocument("author_Aa", "Aa", 1, 1),
                metaDocument("author_aäa", "aäa", 5, 2), metaDocument("author_aÄb", "aÄb", 5, 3),
                metaDocument("author_ab", "ab", 5, 4), metaDocument("author_Ac", "Ac", 3, 5));
        assertEquals(expectedMetaDocuments, documents);
    }


    @Test
    public void checkMetaDocumentsForMultipleFacetFields(){
        List<FacetFieldEntry> allFacets = Lists.newArrayList();
        allFacets.addAll(facetFieldEntriesForPublisher);
        allFacets.addAll(facetFieldEntriesForAuthor);
        List<MetaDocument> documents = convert(allFacets);
        List<MetaDocument> expectedMetaDocuments = Lists.newArrayList(metaDocument("author_Aa", "Aa", 1, 1), metaDocument("publisher_aaÄä", "aaÄä", 2, 2),
                metaDocument("author_aäa", "aäa", 5, 3), metaDocument("author_aÄb", "aÄb", 5, 4),
                metaDocument("publisher_ab", "ab", 10, 5), metaDocument("author_Ac", "Ac", 3, 6));
        assertEquals(expectedMetaDocuments, documents);
    }


    private MetaDocument metaDocument(String id, String author, int counter, int alphabeticPositionAuthor) {
        MetaDocument document = new MetaDocument();
        document.setId(id);
        document.setAuthor(author);
        document.setAlphabeticPositionAuthor(alphabeticPositionAuthor);
        document.setCounter(counter);
        return document;
    }

    private List<MetaDocument> convert(List<FacetFieldEntry> facetFieldEntries) {
        return authorConverter.convert(facetFieldEntries);
    }


}