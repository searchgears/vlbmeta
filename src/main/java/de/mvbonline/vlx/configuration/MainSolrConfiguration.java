package de.mvbonline.vlx.configuration;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

import javax.annotation.Resource;

@Configuration
@EnableSolrRepositories(basePackages={"de.mvbonline.vlx.repository.main"}, multicoreSupport=true,
        solrServerRef = "mainSolrServer", solrTemplateRef = "mainSolrTemplate")
public class MainSolrConfiguration {

    static final String SOLR_URL_VLB_MAIN = "vlb.main.solr";

    @Resource
    private Environment environment;

    @Bean
    @Qualifier("mainSolrServer")
    public SolrServer mainSolrServer() throws Exception{
       String solrHost = environment.getRequiredProperty(SOLR_URL_VLB_MAIN);
        return new HttpSolrServer(solrHost);
    }

    @Bean
    @Qualifier("mainSolrTemplate")
    public SolrTemplate mainSolrTemplate(@Qualifier("mainSolrServer") SolrServer server) throws Exception {
        return new SolrTemplate(server);
    }
}


